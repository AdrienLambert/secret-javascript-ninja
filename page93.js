// En mode non strict this vaut l'objet Window
function ninja()
{
    console.log(this);
}
ninja()
// En mode strict this vaut undefined
function samurai()
{
    "use strict";
    console.log(this);
}
samurai()