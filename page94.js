// Méthode pour retourner le context courant
function whatsMyContext() {
    return this;
}

// Le context courant sera l'objet Window
console.log(whatsMyContext());

// Ici le context courant sera l'objet Window
var getMyThis = whatsMyContext;
console.log(getMyThis());

// Le context courant sera l'objet Ninja
var ninja = {
    getMyThis: whatsMyContext
};
console.log(ninja.getMyThis());

// Le context courante sera l'objet Ninja2
var ninja2 = {
    getMyThis: whatsMyContext
};
console.log(ninja2.getMyThis());