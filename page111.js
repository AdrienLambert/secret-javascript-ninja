var button = {
    clicked: false,
    click: function () {
        this.clicked = true;
        console.log(this);
        console.log(button.clicked);
    }
};
var elem = document.getElementById("test");
elem.addEventListener("click", button.click.bind(button));
var boundFunction = button.click.bind(button);
console.log(boundFunction);