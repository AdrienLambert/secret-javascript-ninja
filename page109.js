var button = {
    clicked: false,
    click: () => {
        this.clicked = true;
        console.log(button.clicked);
        console.log(this);
    }
}

var elem = document.getElementById("test");
elem.addEventListener("click", button.click);