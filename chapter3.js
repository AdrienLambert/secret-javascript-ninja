function isPrime(value) {

    if (!isPrime.answers) {
        isPrime.answers = {};
    }

    if (isPrime.answers[value] !== undefined) {
        return isPrime.answers[value];
    }

    var prime = value !== 1; // 1 is not a prime

    for (var i = 2; i < value; i++) {

        if (value % i === 0) {
            prime = false;
            break;
        }

    }

    return isPrime.answers[value] = prime;
}

console.log(isPrime(5))
console.log(isPrime(10))
console.log(isPrime(5))
console.log(isPrime.answers)

// var store = {
//     nextId: 1,
//     cache: {},
//     add: function(callbackFunction) {
//         if(!callbackFunction.id) {
//             callbackFunction.id = this.nextId++;
//             this.cache[callbackFunction.id] = callbackFunction;
//             return true;
//         }
//     },
// };


// function ninja() {}
// function gi() {}

// console.log(store);
// console.log(store.add(ninja));
// console.log(store.add(gi));
