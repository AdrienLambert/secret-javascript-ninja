function animateIt(elementId) {

    var elem = document.getElementById(elementId);

    var tick = 0;

    var timer = setInterval(function () {
        if (tick < 100) {
            elem.style.left = elem.style.top = tick + "px";
            tick++;
        }
        else {
            clearInterval(timer);
            console.log(tick);
            console.log(elem);
            console.log(timer);
        }
    }, 10);

}

animateIt("box1");